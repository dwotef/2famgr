# 2famgr
### Version: 1.1

This script conveniently keeps your Two Factor Authentication keys encrypted locally with a password, and uses oathtool to get your codes when you need them. It is intentionally kept short and simple so it may be easily audited by anyone who wishes to use it.

By default, the keys are stored in `~/.2famgr/keys` but this can be changed to whatever you wish by changing the `KEYS_PATH` variable.
